import numpy as np
import cv2
import matplotlib as plt
import math


# carrega a imagem
img = cv2.imread("imagens/vaga.png")


# modifica a imagem para tons de cinza (mais do que 50)
imgray = cv2.imread("imagens/vagas.png", 0)


# passa a imagem CINZA pelo algoritmo canny que detecta os contornos presentes na imagem
edges = cv2.Canny(imgray, 300, 510)


# aplica um filtro de noise para suavisar os contornos achados
edges = cv2.GaussianBlur(edges, (5, 5), 0)


# exibe a imagem dos CONTORNOS e so continua o codigo ate clicar em algo no teclado
cv2.imshow('EDGES', edges)
cv2.waitKey(0)


# exibe a imagem INICIAL e so continua o codigo ate clicar em algo no teclado
cv2.imshow('VISAO DA CAMERA', img)
cv2.waitKey(0)


# passa a imagem dos CONTORNOS por esse algoritmo loco ai e
# detecta quais sao as areas(contornos fechados) presente na imagem
thresh = cv2.adaptiveThreshold(edges, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)


# imprime quantas areas foram achadas
print(len(contours) - 1)


# desehna as areas achadas (o 3o argumento eh a posicao da area no vetor 'contours' de areas)
img = cv2.drawContours(img, contours, 5, (156, 56, 72), 3)
img = cv2.drawContours(img, contours, 6, (255, 0, 255), 3)
img = cv2.drawContours(img, contours, 7, (0, 0, 255), 3)
img = cv2.drawContours(img, contours, 8, (0, 255, 0), 3)
img = cv2.drawContours(img, contours, 9, (255, 0, 0), 3)
img = cv2.drawContours(img, contours, 18, (156, 56, 72), 3)
img = cv2.drawContours(img, contours, 22, (255, 0, 255), 3)
img = cv2.drawContours(img, contours, 25, (0, 0, 255), 3)
img = cv2.drawContours(img, contours, 28, (0, 255, 0), 3)
img = cv2.drawContours(img, contours, 30, (255, 0, 0), 3)
img = cv2.drawContours(img, contours, 33, (255, 255, 0), 3)


# exibe a imagem INICIAL com as areas que foram desenhadas
cv2.imshow('VISAO DA CAMERA', img)
cv2.waitKey(0)


# PARA DEBUG
# eu usei essa parte pra descobrir qual eram as posicoes que eu gostaria que fossem desenhadas
# acho que estudando um pouco mais e com uma imagem menos 'poluida' ficaria mais facil de reconhecer as vagas
# ou  a gnt junta um algoritmo de ML pro reconhecimento do que eh vaga ser mais preciso
# ou sl kkkk
#
#
# for i in range(len(contours)):
#     img = cv2.drawContours(img, contours, i, (255, 0, 0), 3)
#     print(i)
#     cv2.imshow('VISAO DA CAMERA', img)
#     cv2.waitKey(0)
